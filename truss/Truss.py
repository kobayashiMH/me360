import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve

import pymongo
from pymongo import MongoClient
from pprint import pprint

class Truss:
    '''
    Class for finite element modeling of trusses
    '''

    def __init__(self, name='model', dim=2):
        self.__client = MongoClient()
        self.__client.drop_database(name)
        self.__model = self.__client[name]
        self.__truss = self.__model['truss']
        self.__joints = self.__truss['joints']
        self.__members = self.__truss['members']
        self.__properties = self.__truss['properties']
        self.__materials = self.__model['materials']
        self.__loads = self.__model['loads']
        self.__restraints = self.__model['restraints']
        self.__solver = None
        self.__displacements = None
        self.__stresses = None
        self.__dimension = dim

    def get_model(self):
        return self.__model

    def get_truss(self):
        return self.__truss

    def add_joint(self, joint):
        self.__joints.insert_one(joint)

    def add_many_joints(self, joints):
        self.__joints.insert_many(joints)

    def get_joints(self):
        return self.__joints

    def add_member(self, member):
        self.__members.insert_one(member)

    def add_many_members(self, members):
        self.__members.insert_many(members)

    def get_members(self):
        return self.__members

    def add_property(self, prop):
        self.__properties.insert_one(prop)

    def add_many_properties(self, properties):
        self.__properties.insert_many(properties)

    def get_properties(self):
        return self.__properties

    def add_material(self, material):
        self.__materials.insert_one(material)

    def add_many_materials(self, materials):
        self.__materials.insert_many(materials)

    def get_materials(self):
        return self.__materials

    def add_load(self, load):
        self.__loads.insert_one(load)

    def add_many_loads(self, loads):
        self.__loads.insert_many(loads)

    def get_loads(self):
        return self.__loads

    def add_restraint(self, restraint):
        self.__restraints.insert_one(restraint)

    def add_many_restraints(self, restraints):
        self.__restraints.insert_many(restraints)

    def get_restraints(self):
        return self.__restraints

    def set_solver(self, solver):
        self.__solver = solver

    def assemble_stiffness_matrix(self):
        def extend_k_block(block, ioff, joff, k_dict):
            for i in range(0, self.__dimension):
                ig = ioff + i
                for j in range(0, self.__dimension):
                    jg = joff + j
                    if (ig, jg) in k_dict:
                        k_dict[(ig, jg)] += block[i, j]
                    else:
                        k_dict[(ig, jg)] = block[i, j]
            return

        k_dict = dict()
        for member in self.__members.find():
            ind_source = member['source']
            ind_target = member['target']

            source_joint = self.__joints.find_one({'_id': ind_source})
            target_joint = self.__joints.find_one({'_id': ind_target})

            a_vector = np.transpose((np.array(target_joint['point']) - np.array(source_joint['point'])).reshape(1,2))

            a = np.linalg.norm(a_vector)

            property = self.__properties.find_one({'_id': member['property id']})
            area = property['area']
            material = self.__materials.find_one({'_id': property['material id']})
            young_modulus = material['Young modulus']

            k_block = (area*young_modulus/a**3)*np.matmul(a_vector, np.transpose(a_vector))

            i_off = self.__dimension * ind_source
            j_off = self.__dimension * ind_target

            extend_k_block(k_block, i_off, i_off, k_dict)
            extend_k_block(-k_block, i_off, j_off, k_dict)
            extend_k_block(-k_block, j_off, i_off, k_dict)
            extend_k_block(k_block, j_off, j_off, k_dict)

        rows = []
        cols = []
        data = []
        for (i, j), d in k_dict.items():
            rows.append(i)
            cols.append(j)
            data.append(d)

        return csr_matrix((data, (rows, cols)))

    def assemble_load_vector(self, load_case, matrix_shape):
        f_dict = dict()
        for load in self.__loads.find({"load case": load_case}):
            j_id = load["joint id"]
            force = load["force"]
            for i in range(self.__dimension):
                ig = i + self.__dimension * j_id
                if ig in f_dict:
                    f_dict[ig] += force[i]
                else:
                    f_dict[ig] = force[i]

        rows = []
        cols = []
        data = []
        for i, d in f_dict.items():
            rows.append(i)
            cols.append(0)
            data.append(d)

        return csr_matrix((data, (rows, cols)), shape=(matrix_shape[0], 1))

    def set_csr_row_to_id(self, a_mat, row_ind):
        assert isinstance(a_mat, csr_matrix), 'Not a scipy csr matrix'

        indptr = a_mat.indptr
        values = a_mat.data
        indxs = a_mat.indices

        # get the range of the data that is changed
        rowpa = indptr[row_ind]
        rowpb = indptr[row_ind + 1]

        # new value and its new rowindex
        values[rowpa] = 1.0
        indxs[rowpa] = row_ind

        # number of new zero values
        diffvals = rowpb - rowpa - 1

        # filter the data and indices and adjust the range
        values = np.r_[values[:rowpa + 1], values[rowpb:]]
        indxs = np.r_[indxs[:rowpa + 1], indxs[rowpb:]]
        indptr = np.r_[indptr[:row_ind + 1], indptr[row_ind + 1:] - diffvals]

        # hard set the new sparse data
        a_mat.indptr = indptr
        a_mat.data = values
        a_mat.indices = indxs

        return

    def set_csr_row_to_value(self, csr, row, value=0):
        assert isinstance(csr, csr_matrix), 'Not a scipy csr matrix'
        csr.data[csr.indptr[row]:csr.indptr[row + 1]] = value

        return

    def apply_restraints_to_stiffness_matrix(self, a_mat):
        for r in self.__restraints.find({}):
            self.set_csr_row_to_id(a_mat, r['joint id']*self.__dimension + r['restraint'])

        return

    def apply_restraints_to_load_vector(self, a_mat):
        for r in self.__restraints.find({}):
            self.set_csr_row_to_value(a_mat, r['joint id']*self.__dimension + r['restraint'], 0)

        return


    def solve(self, load_case):
        k_matrix = self.assemble_stiffness_matrix()
        self.apply_restraints_to_stiffness_matrix(k_matrix)

        f_vector = self.assemble_load_vector(load_case, k_matrix.get_shape())
        self.apply_restraints_to_load_vector(f_vector)

        self.__displacements = self.__solver(k_matrix, f_vector)

        return

    def get_displacements(self):
        return self.__displacements

    def get_stresses(self):
        return self.__stresses


if __name__ == "__main__":
    model = Truss(name='test-model')

    j = [
        {
            "_id": 0,
            "point": [0.0, 0.0]
        },
        {
            "_id": 1,
            "point": [1.0, 0.0]
        },
        {
            "_id": 2,
            "point": [2.0, 1.0]
        },
        {
            "_id": 3,
            "point": [1.0, 1.0]
        },
        {
            "_id": 4,
            "point": [0.0, 1.0]
        }

    ]
    model.add_many_joints(j)

    m = [
        {
            "_id": 0,
            "source": 0,
            "target": 1,
            "property id": 0
        },
        {
            "_id": 1,
            "source": 1,
            "target": 2,
            "property id": 0
        },
        {
            "_id": 2,
            "source": 2,
            "target": 3,
            "property id": 0
        },
        {
            "_id": 3,
            "source": 3,
            "target": 4,
            "property id": 0
        },
        {
            "_id": 4,
            "source": 4,
            "target": 0,
            "property id": 0
        },
        {
            "_id": 5,
            "source": 4,
            "target": 1,
            "property id": 0
        },
        {
            "_id": 6,
            "source": 3,
            "target": 1,
            "property id": 0
        }
    ]
    model.add_many_members(m)

    p = {
        "_id": 0,
        "area": 1.e-4,
        "material id": 0
    }
    model.add_property(p)

    mat = {
        "_id": 0,
        "name": "steel",
        "Young modulus": 250e9
    }
    model.add_material(mat)

    l = {
        "_id": 0,
        "load case": "tip load",
        "joint id": 2,
        "force": [0.0, -900.0]
    }
    model.add_load(l)

    r = [
        {
            "_id": 0,
            "joint id": 0,
            "restraint": 0
        },
        {
            "_id": 1,
            "joint id": 4,
            "restraint": 0
        },
        {
            "_id": 2,
            "joint id": 4,
            "restraint": 1
        }
    ]
    model.add_many_restraints(r)

    model.set_solver(spsolve)

    model.solve("tip load")

    print(model.get_displacements())
