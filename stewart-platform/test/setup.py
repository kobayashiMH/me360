import json
import sys

sys.path.append('..')

test_module = sys.argv[1]
module_cell = 3
with open(test_module) as json_file:
    module_doc = json.load(json_file)
    module_py = module_doc['cells'][module_cell]
with open('foobar.py','w') as module_file:
    for line in module_py['source']:
        if 'import' in line:
            with open('import.tag', 'w') as cheat_file:
                cheat_file.write('The following cell uses import: {}'.format(module_py))
        module_file.write(line)
