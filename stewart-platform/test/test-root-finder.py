from foobar import root_finder

import unittest
from math import *
import json
import sys
import scipy.optimize as spo

sys.path.append('..')

def _root_finder(func, a, b):
    return spo.brentq(func, a, b)

class TestFindRoot(unittest.TestCase):

    def setUp(self):
        return

    def test_polynomial(self):
        f = lambda x: 3*x**2 - 2*x - 10
        a = 2
        b = 2.2
        # test root
        self.assertAlmostEqual(_root_finder(f, a, b), root_finder(f, a, b))

    def test_rational(self):
        M = 200
        f = lambda x: 1/2 - 1/(1 + M*abs(x - 1.05))
        a = 1.05 - (1/M)*1.1
        b = 1.05 + (1/M)*0.9
        # test root
        self.assertAlmostEqual(_root_finder(f, a, b), root_finder(f, a, b))

    def test_transcendental(self):
        f = lambda x: sin(x) - cos(x)
        a = 0
        b = pi
        # test root
        self.assertAlmostEqual(_root_finder(f, a, b), root_finder(f, a, b))
