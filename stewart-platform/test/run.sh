#! /bin/bash

rm -rf __pycache__ foobar.py import.tag test-root-finder.out grades.out

for file in $( find .. -maxdepth 1 -name "*.ipynb" )
do
	echo ${file} >> grades.out
	python3 setup.py ${file}
	python3 -m unittest test-root-finder.py &> test-root-finder.out
	head -1 test-root-finder.out | awk -F. '{print "Number of rights: " NF-1}' >> grades.out
done


