from math import *

class StewartPlatform:
    sqrt_2 = sqrt(2)
    sqrt_5 = sqrt(5)
    def __init__(self, x1 = 4, x2 = 0, y2 = 4, 
             L1 = 2, L2 = sqrt_2, L3 = sqrt_2,
             gamma = pi/2, 
             p1 = sqrt_5, p2 = sqrt_5, p3 = sqrt_5):
        self.__x0 = 0
        self.__y0 = 0
        self.__x1 = x1
        self.__y1 = 0
        self.__x2 = x2
        self.__y2 = y2
        self.__L1 = L1
        self.__L2 = L2
        self.__L3 = L3
        self.__gamma = gamma
        self.__p1 = p1
        self.__p2 = p2
        self.__p3 = p3
        
    def N1_N2_D(self, theta):
        A2 = self.__L3*cos(theta) - self.__x1
        B2 = self.__L3*sin(theta)
        A3 = self.__L2*cos(theta + self.__gamma) - self.__x2
        B3 = self.__L2*sin(theta + self.__gamma) - self.__y2
        
        A2_sq = A2*A2
        B2_sq = B2*B2
        A3_sq = A3*A3
        B3_sq = B3*B3
        
        p1_sq = self.__p1*self.__p1
        p2_sq = self.__p2*self.__p2
        p3_sq = self.__p3*self.__p3
        
        factor1 = p2_sq - p1_sq - A2_sq - B2_sq
        factor2 = p3_sq - p1_sq - A3_sq - B3_sq
        
        N1 = B3*factor1 - B2*factor2
        N2 = -A3*factor1 + A2*factor2
        
        D = 2*(A2*B3 - B2*A3)
               
        return N1, N2, D
    
    def __call__(self, theta):
        N1, N2, D = self.N1_N2_D(theta)
        return N1*N1 + N2*N2 - self.__p1*self.__p1*D*D
    
    def set_p1(self, p1):
        self.__p1 = p1
        
    def set_p2(self, p2):
        self.__p2 = p2
        
    def set_p3(self, p3):
        self.__p3 = p3
    
    def set_p(self, p):
        self.__p1 = p[0]
        self.__p2 = p[1]
        self.__p3 = p[2]
    
    def xy(self, theta):
        N1, N2, D = self.N1_N2_D(theta)
        return N1/D, N2/D
    
    def plot_pose(self, theta, fig, x_lim = None, y_lim = None):
        xP, yP = self.xy(theta)

        x3 = xP + self.__L3*cos(theta)
        y3 = yP + self.__L3*sin(theta)

        x4 = xP + self.__L2*cos(theta + self.__gamma)
        y4 = yP + self.__L2*sin(theta + self.__gamma)

        eps = 1e-10
        assert abs((xP*xP + yP*yP)/(self.__p1*self.__p1) - 1) < eps, 'Error in strut length p1'
        assert abs(((x3 - self.__x1)**2 + (y3 - self.__y1)**2)/(self.__p2*self.__p2) - 1) < eps, 'Error in strut length p2'
        assert abs(((self.__x2 - x4)**2 + (self.__y2 - y4)**2)/(self.__p3*self.__p3) - 1) < eps, 'Error in strut length p3'

        stw_thick = 4.
        if x_lim is None:
            if y_lim is None:
                ax = fig.add_subplot(111, autoscale_on=True)
            else:
                ax = fig.add_subplot(111, autoscale_on=False, ylim = y_lim)
        elif y_lim is None:
            ax = fig.add_subplot(111, autoscale_on=False, xlim = x_lim)
        else:
            ax = fig.add_subplot(111, autoscale_on=False, xlim = x_lim, ylim = y_lim)
        
        ax.plot([self.__x0, xP], [self.__y0, yP],'r')
        ax.plot([self.__x2, x4], [self.__y2, y4], 'r')
        ax.plot([x3, self.__x1], [y3, self.__y1], 'r')
        ax.plot([xP, x3], [yP, y3],'b',linewidth = stw_thick)
        ax.plot([x3, x4], [y3, y4],'b',linewidth = stw_thick)
        ax.plot([x4, xP], [y4, yP],'b',linewidth = stw_thick)
        ax.plot(self.__x0, self.__y0, 'ow')
        ax.plot(self.__x1, self.__y1, 'ow')
        ax.plot(self.__x2, self.__y2, 'ow')
        ax.plot(x3, y3, 'ow')
        ax.plot(x4, y4, 'ow')
        ax.plot(xP, yP, 'ow')
        
        return ax.lines
